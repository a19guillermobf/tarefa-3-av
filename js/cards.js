/**
 * A ideia é recolher os atributos data-imagem, comprovar se ponhem nom, e se ponhem nom
 * asignar-lhe ao div imagem a classe imagem-card-nom e ao contido a classe desc-card-alt
 * 
 * Entom, recolho umha lista de elementos pola classe imagem-card
 * Avalio contido do atributo data-imagem
 * Se procede engado as classes comentadas anteriormente, umha ao próprio elemento
 * e outra ao segundo irmao
 */

function ocultarImagem(){
    let elementos = document.getElementsByClassName("imagem-card");
    console.log(elementos);
    for(let i=0; i<elementos.length; i++){
        if(elementos[i].dataset.imagem=="nom"){
            elementos[i].className+=" imagem-card-nom";
            /**
             * Primeiro selecciona o pai e logo o terceiro filho
             */
            elementos[i].parentNode.childNodes[5].className+=" desc-card-alt";
        }
        /**
         * Se o elemento está marcado como data-video=si fai que se veja o logo do play
         */
        if(elementos[i].dataset.video=="si"){
            elementos[i].firstChild.style.display="inherit";
        }
    }
}

/**
 * Nom entendo moi bem o porque, pero se aqui utilizo a funçom
 * para que a página esteja carregada e meto isto, nom fai os cambios
 * assí que tem que ir fora e ademais tamém nom HTML ao final de todo, se o quito dalgúm destes sítios peta
 */
ocultarImagem();

/**
 * Podería-se tentar fazer que com um eveto click no logo do video te levara a página  à 
 * que corresponde, pero fica aí polo de agora
 */