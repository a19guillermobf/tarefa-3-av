/****************************************************************************************
 *           Función que retrasa a execución de JS ata que a web esté lista
 ****************************************************************************************/
/**
 * Passa-se-lhe umha funçom como argumento e só se executa cando remate de carregar todos os elementos
 */
 function docReady(fn) {
    // Com readyState comprova se o documento está carregado, se nom fai um timeout para chamar
    //cando esteja listo. 
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}


/**
 * Funçom que quita e pom a classe ocultar ao elemento com id=menu-principal
 */
function toogleHideMenu(){
    let menu = document.getElementById("menu-principal");
    console.log(menu);
    menu.classList.toggle("ocultar");
}

/**
 * Funçom que restablece a classe ocultar ao menú cando a pantalla passa de 1200px
 * de ancho, que é a medida que tenho posto para as mediaQueryes.
 * Assí, se se fai um aumento da pantalha tendo a classe quitada, despois se se 
 * volve fazer pequena o menú aparece oculto. Se nom seguiria-se mostrando, e nom 
 * parece que isso seja correcto.
 */
function reporClasseOcutlar(){
    let menu = document.getElementById("menu-principal");
    /**
     * Como só vai ter esta classe podo fazer assí. Se nom terria que percorrer a classList
     * para ver se tem a classe ocultar. Se nom a tem engado-lha
     */
    if((menu.classList[0]==undefined) && (window.innerWidth>=1200)){
        console.log("Ocultou");
        menu.classList.add("ocultar");
    }
}
/**
 * Cando a página este carregada engade o eventListener para cando se clicke o botom
 * do menú que o mostre ou o oculte.
 * Tamém se engade um eventListener para cando se fai resize a pantalha que verifique 
 * se tem que repor a classe ocultar ou nom
 */
docReady(()=>{
    console.log("Entrou em docready")
    let botom = document.getElementById("menu-movil");
    botom.addEventListener('click', toogleHideMenu, false);
    window.addEventListener('resize', reporClasseOcutlar, false);
});

