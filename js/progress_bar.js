/**
 * Funçom para a progress bar
 */
function progressBar(numeroProgresso){
        /**
     * Variável que recolhe todas as barras (classe barra_in) que hai no html, como umha coleçom HTML se nom lembro mal
     */
    let barras = document.getElementsByClassName('barra_in');
    /**
     * Bucle para percorrer a anterior lista e estabelecer estilo width, que é 
     * o que vai marcar até onde se enche a barra, co valor marcado no html.
     * Tamém modifica o texto para que cadre com esse valor
     */
    /**
     * DATASET
     * Vou fazer isto com dataset, a ver se me sae. O conto é que para acceder ao valor
     * do atributo dataset, neste caso data-width, hai que acceder ao elemento, 
     * neste caso fago-o polo nome da classe. e logo o valor do elemento é
     * elemento.dataset.width ou como seja que tenhas posto, lembra que se converte a camelCase, 
     * e que se elimina o data- do princípio, se fora data-width-primario, sería widthPrimario.
     */
     console.log(barras[0].dataset.width);
    for(let i =0;i<barras.length;i++)
    {
        let porcentagem = (barras[i].dataset.width);
        /**
         * Obtém o valor do dataset e logo cambia-lhe o conteúdo e os estilos ao elemento
         */
        barras[i].innerHTML=porcentagem+"%";
        barras[i].style.width = porcentagem+'%';
    }
}

/**
 * Agora vou fazer outra funçom que modifique o dataset, recibe um valor como parámetro
 * e asigna-lho aos elementos co atributo dataset
 */
function mudarDataWidth(n){
    let barras = document.getElementsByClassName('barra_in');
    for(let i=0; i<barras.length; i++){
        barras[i].dataset.width=n;
    }
}

mudarDataWidth(75);
progressBar();

/**
 * para os data-* disque chamados dataset
 * https://developer.mozilla.org/es/docs/Web/API/HTMLOrForeignElement/dataset
 * https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes
 * 
 * Para event.target
 * https://developer.mozilla.org/es/docs/Web/API/Event/target
 * https://es.stackoverflow.com/questions/232642/cu%C3%A1l-es-la-diferencia-entre-this-y-e-target-en-javascript
 * 
 */